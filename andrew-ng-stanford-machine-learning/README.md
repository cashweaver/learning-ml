# Andrew Ng's Machine Learning (Stanford) Class

## About

This includes notes and implementations for topics discussed in Andrew Ng's
Machine Learning course for Stanford.

https://www.coursera.org/learn/machine-learning/home/welcome

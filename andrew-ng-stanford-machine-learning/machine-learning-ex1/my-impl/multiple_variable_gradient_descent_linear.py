import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter

# http://mccormickml.com/2014/03/04/gradient-descent-derivation

# Two Variables

loss_function = lambda theta1, theta2: theta1**4 + theta2**2
dtheta1_loss_function = lambda theta1: 4*theta1
dtheta2_loss_function = lambda theta2: 2*theta2
alpha = 0.1

# Usually you start with 0, but since 0 is the minimum here we'll start with 3.
theta1 = 3
theta2 = 3

thetas = []
num_iterations = 10
for _ in range(num_iterations):
    new_theta1 = theta1 - (alpha * dtheta1_loss_function(theta1))
    new_theta2 = theta2 - (alpha * dtheta2_loss_function(theta2))

    theta1 = new_theta1
    theta2 = new_theta2

    thetas.append([theta1, theta2])

thetas = np.asarray(thetas)
print(thetas)

# Plotting the loss function
r = np.arange(-3.0, 3.0, 0.1)
datax = r + np.random.rand(len(r))
print(datax)
X = r
datay = r + (np.random.rand(len(r)) * 10.0)
Y = r
X, Y = np.meshgrid(X, Y)
Z = loss_function(X, Y)

fig, ax = plt.subplots()

CS = ax.contour(X, Y, Z, r)
ax.clabel(CS, inline=1, fontsize=10)

ax.plot(thetas[:,0], thetas[:,1], 'bo')

plt.show()



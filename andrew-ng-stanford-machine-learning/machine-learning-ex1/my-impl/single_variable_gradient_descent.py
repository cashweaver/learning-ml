import numpy as np
import matplotlib.pyplot as plt

# http://mccormickml.com/2014/03/04/gradient-descent-derivation

num_iterations = 10
loss_function = lambda x: x**2
derivative_of_loss_function = lambda x: 2*x
alpha = 0.1

# Usually you start with 0, but since 0 is the minimum here we'll start with 3.
theta = 3

thetas = []
for _ in range(num_iterations):
    theta = theta - (alpha * derivative_of_loss_function(theta))
    thetas.append(theta)

# Thetas
plt.plot(thetas, loss_function(np.asarray(thetas)), 'bo')
# Loss function
plt.plot(np.arange(-3.0, 3.0, 0.1), loss_function(np.arange(-3.0, 3.0, 0.1)), 'k')
plt.show()

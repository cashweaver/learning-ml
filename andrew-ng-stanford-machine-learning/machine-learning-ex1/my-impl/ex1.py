import numpy as np
import matplotlib.pyplot as plt

data1_path = '../ex1/ex1data1.txt'
data2_path = '../ex1/ex1data2.txt'

# Linear Regression with One Variable
# 2.0: Read the data

data1 = np.loadtxt(data1_path, delimiter=',')
#print(data1)

# 2.1: Plotting the data

#plt.plot(data1[:,0], data1[:,1], 'bo')
#plt.xlabel('Population of City (10,000\'s)');
#plt.ylabel('Profit ($10,000\'s)');
#plt.show()

# 2.2: Gradient Descent

ones = np.ones((len(data1[:,0]),1))
X = np.hstack((ones, data1[:,0][np.newaxis].T))

Theta = np.zeros((2, 1))
#print(Theta)
num_iterations = 1500
alpha = 0.01
m = len(data1)
y = data1[:,1]
h = lambda x, theta0, theta1: theta0 + (theta1 * x)
theta0 = 0
theta1 = 0

def calc_cost(x, theta0, theta1):
    m = len(x)

    inner = 0
    for i in range(m):
        inner += (h(x[i], theta0, theta1) - y[i]) ** 2

    return (1 / (2 * m)) * inner


costs = []
for _ in range(num_iterations):
    theta0_inner = 0
    for i in range(m):
        theta0_inner += (h(data1[:,0][i], theta0, theta1) - y[i]) * X[i][0]

    theta1_inner = 0
    for i in range(m):
        theta1_inner += (h(data1[:,0][i], theta0, theta1) - y[i]) * X[i][1]

    theta0 = theta0 - (alpha * (1/m) * theta0_inner)
    theta1 = theta1 - (alpha * (1/m) * theta1_inner)
    
    costs.append(calc_cost(data1[:,0], theta0, theta1))


# Plot cost
#plt.plot(np.arange(0, len(costs), 1), costs)
#plt.show()


x = data1[:,0]
y = data1[:,1]
plt.plot(x, y, 'ro')
plt.plot([x.min(), x.max()], [h(x.min(), theta0, theta1), h(x.max(), theta0, theta1)])
plt.show()
